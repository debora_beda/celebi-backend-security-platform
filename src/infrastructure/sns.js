import * as AWS from 'aws-sdk';

async function publish(subject, message) {
  let sns;
  
  sns = new AWS.SNS({ region: 'us-east-1' })
  
  return await sns.publish({
    Subject: subject,
    Message: message,
    TopicArn: 'arn:aws:sns:us-east-1:470845619410:celebi-security-alert'
  }).promise();
}

export { publish } 