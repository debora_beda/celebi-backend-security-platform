import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import alert from '../../interface/alert';
import { serviceLogger } from '../config/service-logger';

const app = express();

app.use(bodyParser.json({ strict: false }));
app.use(function (req, res, next) {
  serviceLogger.getInstance();
  serviceLogger.configureContext({
    applicationName: 'celebi-backend-assets-platform',
    requestId: req.headers['x-request-id']
  })
  next();
});
app.use(cors());
app.use(alert);

export default app;
