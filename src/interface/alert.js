import Router from 'express';
import { alert } from '../application/alert';

const alertRoutes = Router();

alertRoutes.post("/alert", async (request, response) => {
  await alert(request, response);
})

export default alertRoutes;