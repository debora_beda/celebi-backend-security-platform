import { publish } from '../infrastructure/sns';
import { serviceLogger } from '../infrastructure/config/service-logger';

const logger = serviceLogger.getInstance();

async function alert(request, response) {
  logger.info('Send alert');

  const req = request.body;
  const subject = req.subject;
  const message = req.message;

  try {
    const result = await publish(subject, message);
    response.json({ messageId: result.MessageId});
  } catch (error) {
    console.log(error);
    response.status(400).json({ error: 'Could send message' });
  }

}

export { alert };